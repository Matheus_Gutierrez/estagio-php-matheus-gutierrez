<?php
    session_start();
?>
<p>Bem vindo!</p>
<p>Escolha entre uma das opções abaixo.</p>
<?php
    //$_SESSION["conexao"] guarda a senha fornecida para acesso ao banco de dados.
    //$_SESSION["sucesso"] indica se a conexao com o banco de dados foi bem sucedida.
    //A conexao com o banco de dados e verificada a cada carregamento de pagina.
    if(!isset($_SESSION["conexao"])){
        echo "<p>Certifique-se de criar um banco de dados antes de utilizar as funcionalidades desse programa.</p>";
    }
    else if (!isset($_SESSION["sucesso"])){
        echo "<p>Não foi possivel se conectar com o banco de dados.</p>";
    }
?>
<a href="gera_bd.php">Banco de Dados</a> <br>
<a href="cadastrar_pessoa.php">Cadastrar Pessoa</a> <br>
<a href="buscar_pessoa.php">Buscar Pessoa</a> <br>
<a href="modificar_pessoa.php">Modificar Pessoa</a> <br>
<a href="excluir_pessoa.php">Excluir Pessoa</a> <br>