<?php
    class bd{
        public $conn = null;
        public $status = 0;
        public function verifica_bd(string $pg_user, string $pg_pw){
            if ($pg_pw != "0"){
                $this->conn = @pg_connect("host=localhost port=5432 dbname=db_pessoas0000 user=" . $pg_user ." password=" . $pg_pw);
                if (!$this->conn) {
                    //Se a conexao com o db_pessoas0000 nao for possivel, reseta a variavel de sucesso, travando as funcionalidades.
                    unset($_SESSION["sucesso"]);
                    if (!@pg_ping($this->conn)){
                        $this->cria_bd($pg_user, $pg_pw);
                    }
                }
                else{
                    $this->status = 1;
                    $_SESSION["sucesso"] = 1;
                }
            }
        }
        private function cria_bd(string $pg_user, string $pg_pw){
            $this->conn = @pg_connect("host=localhost port=5432 user=" . $pg_user ." password=" . $pg_pw);
            if (!$this->conn) {
                echo "<p> Não foi possível acessar o banco de dados.</p>";
                if($pg_pw != "0"){
                    echo "<p> Por favor crie um banco de dados para o cadastro de pessoas.</p>";
                }
            }
            else{
                //Cria o banco de dados db_pessoas0000 no servidor postgres para armazenar os dados das pessoas e a quantidade de ids ja criados.
                $query = "CREATE DATABASE db_pessoas0000;";
                $resultado_bd = pg_exec($this->conn, $query);
                $this->conn = @pg_connect("host=localhost port=5432 dbname=db_pessoas0000 user=" . $pg_user ." password=" . $pg_pw);
                //Dados das pessoas.
                $query_tabela = "CREATE TABLE pessoa(
                    id INT NOT NULL,
                    nome VARCHAR (60) NOT NULL,
                    telefone VARCHAR (11) NOT NULL,
                    cpf VARCHAR (11) NOT NULL,
                    data_nasc VARCHAR (10) NOT NULL,
                    sexo VARCHAR (10) NOT NULL,
                    PRIMARY KEY (id)
                );";
                $resultado_tabela = pg_exec($this->conn, $query_tabela);
                //Esta tabela guarda a quantidade de ids ja criados.
                $query_tabela_controle = "CREATE TABLE controle(
                    id VARCHAR(2) NOT NULL,
                    ids_totais_pessoas INT NOT NULL,
                    PRIMARY KEY (id)
                );";
                $resultado_tabela_controle = pg_exec($this->conn, $query_tabela_controle);
                //Valores padrao para a tabela que armazena a quantidade ids.
                $query_controle = "INSERT INTO controle VALUES ('01', 0)";
                $resultado_controle = pg_exec($this->conn, $query_controle);
                $_SESSION["sucesso"] = 1;
                echo "<p>Banco de dados criado com sucesso.</p>";
            }
        }
    }
?>

