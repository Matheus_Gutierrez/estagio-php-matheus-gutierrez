<?php
    session_start();
    require_once "classe_bd.php";
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <fieldset>
        <legend>Criar Banco de Dados</legend>
        <legend>É necessario a criação de um banco de dados para armazenar os dados de cada pessoa.</legend>
        <input type="hidden" value="1" name="criado">
        <p>Usuario postgres: <br> <input type="text" name="usuario" /></p>
        <p>Senha: <br> <input type="password" name="acesso" /></p>
        <p> <br> <input type="submit" value="Criar" name="create"/> <input type="submit" value="Reiniciar" name="reset"/></p>
        <?php
            //$_SESSION["conexao"] guarda a senha fornecida para acesso ao banco de dados.
            //$_SESSION["sucesso"] indica se a conexao com o banco de dados foi bem sucedida.
            if (isset($_POST["criado"]) && isset($_POST["acesso"]) && isset($_POST["usuario"]) && !isset($_SESSION["sucesso"]) && isset($_POST["create"])){
                $_SESSION["usuario"] = $_POST["usuario"];
                $_SESSION["conexao"] = $_POST["acesso"];
                $conexao = new bd();
                $conexao->verifica_bd($_SESSION["usuario"], $_SESSION["conexao"]);
            }
            else if (isset($_SESSION["sucesso"]) && isset($_POST["create"])){
                echo "<p>Você ja configurou o banco de dados.</p>";
            }
            //Comando para testes. E necessario fornecer informacoes de usuario e senha apos reinicio.
            else if (isset($_POST["reset"])){
                echo "<p>Reiniciando.</p>";
                unset($_SESSION["conexao"]);
                unset($_SESSION["sucesso"]);
            }
        ?>
    </fieldset>
</form>
<a href="inicio.php">Retornar</a> <br>

