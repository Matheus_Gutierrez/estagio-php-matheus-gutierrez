Este programa foi desenvolvido sobre a plataforma WAMP (3.1.0 64 bits) utilizando o SGBD PostgreSQL (Versão 11).
A versão do PHP utilizada é 7.0.23.

Para habilitar o seu funcionamento, é necessario realizar as instruções abaixo.

Descomentar (removendo o caractere ";") as seguintes linhas em "\wamp64\bin\apache\apache2.4.27\bin\php.ini":
";extension=php_pdo_pgsql.dll"
";extension=php_pgsql.dll"

Copiar os conteudos deste repositório para uma nova pasta em "\wamp64\www".
Por exemplo "\wamp64\www\nova-pasta".

Ativar o WAMP e o servidor PostgreSQL.

Acessar o programa através do endereço "http://localhost/nova-pasta/inicio.php".

Escolher a opção "Banco de Dados" e fornecer nome de usuario e senha de acesso ao servidor PostgreSQL, para
que seja criado um novo banco de dados (chamado "db_pessoas0000") onde serão armazenadas as informações sobre
cada pessoa.

Pronto, agora o programa está pronto para ser utilizado.

------------------------------------------------------------------------------------------------------------