<?php
    class pessoa{
        public $id = 0;
        public $nome = "";
        public $telefone = "";
        public $cpf = "";
        public $data_nasc = "";
        public $sexo = "";
        //Insere uma pessoa e suas informacoes no bd.
        public function inserir_pessoa(bd $conexao){
            //Este if garante que o limite de caracteres n seja excedido.
            if (strlen($this->nome) <= 60 && strlen($this->telefone) <= 11 && strlen($this->cpf) <= 11){
                try{
                    //Verifica o total de ids ja criados, para que seja possivel atribuir um id valido a uma pessoa.
                    $this->total_pessoas($conexao);
                    $query = "INSERT INTO pessoa VALUES (" . $this->id . ", '" . $this->nome . "', '" . $this->telefone . "', '" . $this->cpf . "', '" . 
                        $this->data_nasc . "', '" . $this->sexo . "');";
                    $resultado = pg_exec($conexao->conn, $query);
                    //Incrementa o total de ids de pessoas ja criados.
                    $total = $this->id + 1;
                    $query_update = "UPDATE controle SET ids_totais_pessoas = " . $total . " WHERE id = '01';";
                    $resultado_update = pg_exec($conexao->conn, $query_update);
                    echo "<p>" . $this->nome . " foi cadastrado com sucesso</p>";
                }
                catch (Exception $e){
                    echo "<p>Erro ao cadastrar pessoa</p>";
                }
            }
            else{
                echo "<p>Algumas informações excedem o limite de caracteres.</p>";
            }
        }
        public function modificar_pessoa(bd $conexao){
            //Uma busca e realizada para verificar se o id enviado corresponde com um id existente.
            $query_busca = "select nome from pessoa where id = " . $this->id . ";";
            $busca = pg_exec($conexao->conn, $query_busca);
            $resultado_busca = pg_fetch_assoc($busca);
            //Este if verifica se foi encontrada uma pessoa com o id enviado e garante que o limite de caracteres n seja excedido.
            if(pg_num_rows($busca) != 0 && strlen($this->nome) <= 60 && strlen($this->telefone) <= 11 && strlen($this->cpf) <= 11){
                $nome = $resultado_busca["nome"];
                try{
                    $query = "UPDATE pessoa SET nome = '" . $this->nome . "', telefone = '" . $this->telefone . "', cpf = '" . $this->cpf 
                        . "', data_nasc = '" . $this->data_nasc . "', sexo = '" . $this->sexo . "' WHERE id = " . $this->id . ";";
                    $resultado = pg_exec($conexao->conn, $query);
                    echo "<p>As informações de '" . $nome . "' foram modificadas com sucesso.</p>";
                }
                catch (Exception $e){
                    echo "<p>Erro ao modificar informações</p>";
                }
            }
            else if(pg_num_rows($busca) == 0){
                echo "<p>ID de pessoa invalido</p>";
            }
            else{
                echo "<p>Algumas informações excedem o limite de caracteres.</p>";
            }
        }
        public function excluir_pessoa(bd $conexao){
            //Uma busca e realizada para verificar se o id enviado corresponde com um id existente.
            $query_busca = "select nome from pessoa where id = " . $this->id . ";";
            $busca = pg_exec($conexao->conn, $query_busca);
            $resultado_busca = pg_fetch_assoc($busca);
            //Este if verifica se foi encontrada uma pessoa com o id enviado.
            if(pg_num_rows($busca) != 0){
                $nome = $resultado_busca["nome"];
                try{
                    $query_deleta = "DELETE FROM pessoa WHERE id = " . $this->id . ";";
                    $resultado = pg_exec($conexao->conn, $query_deleta);
                    echo "<p>As informações de '" . $nome . "' foram excluidas com sucesso.</p>";
                }
                catch (Exception $e){
                    echo "<p>Erro ao excluir pessoa</p>";
                }
            }
            else{
                echo "<p>ID de pessoa invalido</p>";
            }
        }
        public function buscar_pessoa(bd $conexao){
            //Esta busca reune todas as informacoes das pessoas cujo nome seja semelhante ao nome pesquisado.
            $query_busca = "SELECT id, nome, telefone, cpf, data_nasc, sexo FROM pessoa WHERE nome LIKE '%" . $this->nome . "%'";
            $busca = pg_exec($conexao->conn, $query_busca);
            if(pg_num_rows($busca) != 0){
                $total_resultados = pg_num_rows($busca);
                echo "<p>Total de pessoas encontradas: " . $total_resultados . "</p>";
                $i = 0;
                //Mostra as informacoes de cada pessoa cujo nome seja semelhante ao nome pesquisado.
                while ($i < $total_resultados){
                    $resultado_busca = pg_fetch_assoc($busca);
                    $id = $resultado_busca["id"];
                    $nome = $resultado_busca["nome"];
                    $telefone = $resultado_busca["telefone"];
                    $cpf = $resultado_busca["cpf"];
                    $data_nasc = $resultado_busca["data_nasc"];
                    $sexo = $resultado_busca["sexo"];
                    echo "<p>ID: " . $id . " | NOME: " . $nome . " | TELEFONE: " . $telefone . " | CPF: " . $cpf . " | DATA NASCIMENTO: " . $data_nasc
                            . " | SEXO: " . $sexo . " </p>";
                    $i = $i + 1;
                }
            }
            else{
                echo "<p>O nome pesquisado não corresponde ao nome de nenhuma pessoa.</p>";
            }
        }
        public function total_pessoas(bd $conexao){
            //Verifica o total de ids ja criados.
            $query_total = "SELECT ids_totais_pessoas FROM controle WHERE id = '01'";
            $resultado_query = pg_exec($conexao->conn, $query_total);
            $resultado_total = pg_fetch_assoc($resultado_query);
            $this->id = $resultado_total["ids_totais_pessoas"];
        }
    }
?>