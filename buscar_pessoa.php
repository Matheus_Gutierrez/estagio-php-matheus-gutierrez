<?php
    session_start();
    require_once "classe_bd.php";
    require_once "classe_pessoa.php";
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <fieldset>
        <legend>Buscar Pessoa</legend>
        <?php
            //$_SESSION["conexao"] guarda a senha fornecida para acesso ao banco de dados.
            //$_SESSION["sucesso"] indica se a conexao com o banco de dados foi bem sucedida.
            //A conexao com o banco de dados e verificada a cada carregamento de pagina.
            if(!isset($_SESSION["conexao"])){
                echo "<p>Certifique-se de criar um banco de dados antes de utilizar as funcionalidades desse programa.</p>";
            }
            else{
                $conexao = new bd();
                $conexao->verifica_bd($_SESSION["usuario"], $_SESSION["conexao"]);
            }
            if (!isset($_SESSION["sucesso"])){
                echo "<p>Não foi possivel se conectar com o banco de dados.</p>";
            }
        ?>
        <input type="hidden" value="1" name="buscado">
        <p>Serão mostradas todas as pessoas cujo o nome combinar com o parametro de busca.</p>
        <p>Nome: <br> <input type="text" name="p_nome" /></p>
        <p> <br> <input type="submit" value="Buscar"/></p>
        <?php
            //Apos enviar as informacoes, verifica se a conexao com o banco de dados foi bem sucedida, e entao executa a funcao de busca.
            if(isset($_POST["buscado"]) && isset($_SESSION["sucesso"])){
                $buscar_pessoa = new pessoa();
                $buscar_pessoa->nome = $_POST["p_nome"];
                $buscar_pessoa->buscar_pessoa($conexao);
            }
            //Se nao houver conexao
            else if(!isset($_SESSION["sucesso"])){
                echo "<p>Verifique a situação do banco de dados antes de utilizar as funcionalides do programa.</p>";
            }
        ?>
    </fieldset>
</form>
<a href="inicio.php">Retornar</a> <br>

