<?php
    session_start();
    require_once "classe_bd.php";
    require_once "classe_pessoa.php";
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <fieldset>
        <legend>Cadastrar Pessoa</legend>
        <?php
            //$_SESSION["conexao"] guarda a senha fornecida para acesso ao banco de dados.
            //$_SESSION["sucesso"] indica se a conexao com o banco de dados foi bem sucedida.
            //A conexao com o banco de dados e verificada a cada carregamento de pagina.
            if(!isset($_SESSION["conexao"])){
                echo "<p>Certifique-se de criar um banco de dados antes de utilizar as funcionalidades desse programa.</p>";
            }
            else{
                $conexao = new bd();
                $conexao->verifica_bd($_SESSION["usuario"], $_SESSION["conexao"]);
            }
            if(!isset($_SESSION["sucesso"])){
                echo "<p>Não foi possivel se conectar com o banco de dados.</p>";
            }
        ?>
        <input type="hidden" value="1" name="cadastrado">
        <p>Nome: <br> <input type="text" name="p_nome" /></p>
        <p>Telefone (Max 11 numeros): <br> <input type="text" name="p_telefone" /></p>
        <p>CPF (Max 11 caracteres): <br> <input type="text" name="p_cpf" /></p>
        <p>Data Nascimento: <br> <input type="date" name="p_data_nasc" /></p>
        <p>Sexo: </p>
        <select name="p_sexo">
            <option value="M" selected>M</option>
            <option value="F">F</option>
        </select>
        <p> <br> <input type="submit" value="Cadastrar"/></p>
        <?php
            //Apos enviar as informacoes, verifica se a conexao com o banco de dados foi bem sucedida, e entao executa a funcao de cadastro.
            if(isset($_POST["cadastrado"]) && isset($_SESSION["sucesso"])){
                $nova_pessoa = new pessoa();
                $nova_pessoa->nome = $_POST["p_nome"];
                $nova_pessoa->telefone = $_POST["p_telefone"];
                $nova_pessoa->cpf = $_POST["p_cpf"];
                $nova_pessoa->data_nasc = $_POST["p_data_nasc"];
                $nova_pessoa->sexo = $_POST["p_sexo"];
                $nova_pessoa->inserir_pessoa($conexao);
            }
            //Se nao houver conexao
            else if(!isset($_SESSION["sucesso"])){
                echo "<p>Verifique a situação do banco de dados antes de utilizar as funcionalides do programa.</p>";
            }
        ?>
    </fieldset>
</form>
<a href="inicio.php">Retornar</a> <br>