<?php
    session_start();
    require_once "classe_bd.php";
    require_once "classe_pessoa.php";
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <fieldset>
        <legend>Modificar Pessoa</legend>
        <?php
            //$_SESSION["conexao"] guarda a senha fornecida para acesso ao banco de dados.
            //$_SESSION["sucesso"] indica se a conexao com o banco de dados foi bem sucedida.
            //A conexao com o banco de dados e verificada a cada carregamento de pagina.
            if(!isset($_SESSION["conexao"])){
                echo "<p>Certifique-se de criar um banco de dados antes de utilizar as funcionalidades desse programa.</p>";
            }
            else{
                $conexao = new bd();
                $conexao->verifica_bd($_SESSION["usuario"], $_SESSION["conexao"]);
            }
            if (!isset($_SESSION["sucesso"])){
                echo "<p>Não foi possivel se conectar com o banco de dados.</p>";
            }
        ?>
        <input type="hidden" value="1" name="modificado">
        <p>Id da Pessoa: <br> <input type="text" name="p_id" /></p>
        <hr>
        <p>Escreva os novos dados para a pessoa detentora do Id providenciado.</p>
        <p>Nome: <br> <input type="text" name="p_nome" /></p>
        <p>Telefone (Max 11 numeros): <br> <input type="text" name="p_telefone" /></p>
        <p>CPF (Max 11 caracteres): <br> <input type="text" name="p_cpf" /></p>
        <p>Data Nascimento: <br> <input type="date" name="p_data_nasc" /></p>
        <p>Sexo: </p>
        <select name="p_sexo">
            <option value="M">M</option>
            <option value="F">F</option>
        </select>
        <p> <br> <input type="submit" value="Modificar"/></p>
        <?php
            //Apos enviar as informacoes, verifica se a conexao com o banco de dados foi bem sucedida, e entao executa a funcao de modificacao.
            if(isset($_POST["modificado"]) && isset($_SESSION["sucesso"])){
                $modifica_pessoa = new pessoa();
                $modifica_pessoa->id = intval($_POST["p_id"]);
                $modifica_pessoa->nome = $_POST["p_nome"];
                $modifica_pessoa->telefone = $_POST["p_telefone"];
                $modifica_pessoa->cpf = $_POST["p_cpf"];
                $modifica_pessoa->data_nasc = $_POST["p_data_nasc"];
                $modifica_pessoa->sexo = $_POST["p_sexo"];
                $modifica_pessoa->modificar_pessoa($conexao);
            }
            //Se nao houver conexao
            else if(!isset($_SESSION["sucesso"])){
                echo "<p>Verifique a situação do banco de dados antes de utilizar as funcionalides do programa.</p>";
            }
        ?>
    </fieldset>
</form>
<a href="inicio.php">Retornar</a> <br>